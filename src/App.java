import java.util.ArrayList;

public class App {



    public static void main(String[] args) throws Exception {

        Station Hamburg = new Station("Hamburg", 1, 100);
        Station Berlin = new Station("Berlin", 2, 150);
        Station Bremen = new Station("Bremen", 3, 50);
        Station Kiel = new Station("Kiel", 4, 70);
        Station Koln = new Station("Koln", 5, 170);

        ArrayList<Station> listStation = new ArrayList<Station>();

        listStation.add(Hamburg);
        listStation.add(Berlin);
        listStation.add(Bremen);
        listStation.add(Kiel);
        listStation.add(Koln);

        for (Station point : listStation){
            System.out.println(point.nameStation);
        }
   
    }
}
